

 //    Create a function to retrieve and display the first hobby of each individual in the dataset.
 function firstHobby(arrayOfObjects){

    let firstHobbies = [];

    if(typeof (arrayOfObjects) === 'object' && (arrayOfObjects.length > 0)){
        for(let index = 0;index < arrayOfObjects.length;index++){

            firstHobbies.push(arrayOfObjects[index].hobbies[0]);

        }
        return firstHobbies;
    }
    else{
        return [];
    }
   }

   module.exports = firstHobby;
//    Given the dataset of individuals, write a function that accesses and returns the email addresses of all individuals.
function emailAddressOfIndividual(arrayOfObjects){

    if (typeof (arrayOfObjects) === 'object' && (arrayOfObjects.length > 0)){
      let emails = [];
      for(let index = 0;index<arrayOfObjects.length;index++){
        emails.push(arrayOfObjects[index].email);
      }
      return emails;
    }

    else{
        return [];
    }
}

module.exports = emailAddressOfIndividual;
//    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.
  
function specficAgeHobby(arrayOfObjects,age){

    if(typeof(arrayOfObjects) === 'object' && typeof(age) === 'number' && arrayOfObjects.length > 0){
        let personHobbies = []; 
        for(let index = 0;index<arrayOfObjects.length;index++){
            if(arrayOfObjects[index].age === age){
               personHobbies.push(arrayOfObjects[index].hobbies);
            }
         }
        return personHobbies;
    }

    else{
        return [];
    }
}

module.exports = specficAgeHobby;
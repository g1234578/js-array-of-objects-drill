
  //    Create a function that extracts and displays the names of individuals who are students (`isStutdent: true`) and live in Australia.
  
  function namesOfStudents(arrayOfObjects){
    if (typeof (arrayOfObjects) === 'object' && (arrayOfObjects.length > 0)){
        let names = [];
        for(let index = 0;index<arrayOfObjects.length;index++){
          if(arrayOfObjects[index].isStudent && arrayOfObjects[index].country === 'Australia'){
            names.push(arrayOfObjects[index].name);
          }
        }
        return names;
    }

    else{
        return [];
    }
    
}

module.exports = namesOfStudents;
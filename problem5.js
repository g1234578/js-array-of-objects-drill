function printAgeOfAll(arrayOfObjects) {

    if (typeof (arrayOfObjects) === 'object' && (arrayOfObjects.length > 0)) {

        let size = arrayOfObjects.length;

        let ageList = [];

        for (let index = 0; index < size; index++) {

            ageList.push(arrayOfObjects[index].age)
        }

        return ageList;
    } else {
        return [];
    }

};


module.exports = printAgeOfAll;

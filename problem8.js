//    Implement a loop to access and log the city and country of each individual in the dataset.
function logCityAndCountry(arrayOfObjects) {
    
    if(typeof (arrayOfObjects) == 'object' && (arrayOfObjects.length > 0)){
        let cityAndCountyOfEachPerson = [];

        for(let index = 0;index<arrayOfObjects.length;index++){
            cityAndCountyOfEachPerson.push({
                city : arrayOfObjects[index].city,
                country : arrayOfObjects[index].country
            });
        }
        return cityAndCountyOfEachPerson;
    }
    else{
        return [];
    }
}

module.exports = logCityAndCountry;
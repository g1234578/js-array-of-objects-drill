const {arrayOfObjects} = require('../arrayOfObjects.js');
const emailAddressOfIndividual = require('../problem1.js');

const emails = emailAddressOfIndividual(arrayOfObjects);

if(emails != []){
    console.log(`Emails of all persons : \n${emails}`);
}

else{
    console.log("data is empty");
}
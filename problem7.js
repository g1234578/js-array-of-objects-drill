//    Write a function that accesses and prints the names and email addresses of individuals aged 25.
  function logNamesAndEmailsOfGivenAge(arrayOfObjects,age) {
   
    if(typeof (arrayOfObjects) == 'object' && (arrayOfObjects.length > 0) && typeof(age) === 'number'){
        let nameAndEmailOfGivenAge = null;
        for(let index =0;index<arrayOfObjects.length;index++){
            if(arrayOfObjects[index].age === age){
                nameAndEmailOfGivenAge = {
                    name : arrayOfObjects[index].name,
                    email: arrayOfObjects[index].email
                }
            }
        }
        return nameAndEmailOfGivenAge;
    }

        else{
            return null;
        }

    }

    module.exports = logNamesAndEmailsOfGivenAge;

function studentDetailAtGivenIndex(arrayOfObjects, givenIndex) {

    if (typeof (arrayOfObjects) == 'object' && (arrayOfObjects.length > 0) && typeof(givenIndex) == 'number' ) {
        let studentDetails = null;
        if(givenIndex >=0 && givenIndex < arrayOfObjects.length){
            studentDetails = {
                name : arrayOfObjects[givenIndex].name,
                city : arrayOfObjects[givenIndex].city
            }
        }
        return studentDetails;
    }else { 
        return null; // or any appropriate value indicating the index is out of range
        }

};


module.exports = studentDetailAtGivenIndex;
